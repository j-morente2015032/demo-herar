import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CobradoresPage } from '../pages/administrator/cobradores/cobradores';
import { AdministratorPage } from '../pages/administrator/administrator';
import { FormCobradoresPage } from '../pages/administrator/cobradores/form-cobradores/form-cobradores';
import { CobrosPage } from '../pages/administrator/cobros/cobros';
import { FormCobrosPage } from '../pages/administrator/cobros/form-cobros/form-cobros';
import { Geolocation } from '../../node_modules/@ionic-native/geolocation';
import { CobrosClientPage } from '../pages/client/cobros-client/cobros-client';
import { ClientPage } from '../pages/client/client';
import { FormCobrosClientPage } from '../pages/client/cobros-client/form-cobros-client/form-cobros-client';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CobradoresPage,
    AdministratorPage,
    FormCobradoresPage,
    CobrosPage,
    FormCobrosPage,
    CobrosClientPage,
    ClientPage,
    FormCobrosClientPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CobradoresPage,
    AdministratorPage,
    FormCobradoresPage,
    CobrosPage,
    FormCobrosPage,
    CobrosClientPage,
    ClientPage,
    FormCobrosClientPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
