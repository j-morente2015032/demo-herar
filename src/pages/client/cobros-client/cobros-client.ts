import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { FormCobrosClientPage } from './form-cobros-client/form-cobros-client';

@Component({
  selector: 'cobros-client',
  templateUrl: 'cobros-client.html'
})
export class CobrosClientPage {
  public data:any[] = [];
  public parameter:any;
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public modal: ModalController,
    public navParams: NavParams,
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getAll();
  }

  getAll() {
    this.data = [];
    this.data = JSON.parse(localStorage.getItem('DemoCobros'));
  }

  //AGREGAR
  detailForm(parameter:any) {
    let chooseModal = this.modal.create(FormCobrosClientPage, { parameter} );
    chooseModal.present();
  }


}
