import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { ViewController } from '../../../../../node_modules/ionic-angular/navigation/view-controller';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';

//JQUERY
declare var $:any;
declare var google;

@Component({
  selector: 'form-cobros-client',
  templateUrl: 'form-cobros-client.html'
})
export class FormCobrosClientPage {
  public datalocal:any[] = [];
  private map: any;
  private parameter: any;
  public data = {
    id: '',
    idCobrador: '',
    monto: '',
    direccion: '',
    acontecimiento: '',
    fechaPago: '',
    longitude: '',
    latitude: '',
  }

  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public view: ViewController,
    public geolocation: Geolocation,
    public navParams: NavParams,
  ) {
    this.data = this.navParams.get('parameter')
    this.parameter = this.navParams.get('parameter')
    this.datalocal = JSON.parse(localStorage.getItem('DemoCobros'));
    this.getPosition();
  }

  public closeModal() {
    this.view.dismiss('Close');
  }

  getPosition():any{
    this.geolocation.getCurrentPosition().then(resp => {
      this.loadMap(resp)
     }).catch(error => {
      console.log('Error getting location', error);
    });
  }

  //CARGAR MAPA
  public loadMap(position: Geoposition) {
  let latitude = position.coords.latitude;
  let longitude = position.coords.longitude;
  this.data.latitude = latitude.toString();
  this.data.longitude = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.data.latitude = evt.latLng.lat();
      this.data.longitude = evt.latLng.lng();
    });
  }

  public saveChanges() {
    if(this.datalocal) {
        this.datalocal.push(this.data)
    } else {
        this.datalocal = [this.data]
    }
    localStorage.setItem('DemoCobros', JSON.stringify(this.datalocal));
    this.view.dismiss('Aceptado');
  }

}
