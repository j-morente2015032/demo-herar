import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AdministratorPage } from '../administrator/administrator';
import { ClientPage } from '../client/client';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  //PROPIEDADES
  public user:any = {
    username:"",
    password: ""
  }

  constructor(public navCtrl: NavController) {

  }

  //LOGIN
  logIn(){
    if(this.user.username == 'admin' && this.user.password == 'admin') {
      this.navCtrl.setRoot(AdministratorPage);
    } else if(this.user.username == 'cobrador' && this.user.password == 'cobrador') {
      this.navCtrl.setRoot(ClientPage);
    }
  }

}
