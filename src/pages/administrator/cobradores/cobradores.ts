import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { FormCobradoresPage } from './form-cobradores/form-cobradores';
import { CobrosPage } from '../cobros/cobros';

@Component({
  selector: 'cobradores',
  templateUrl: 'cobradores.html'
})
export class CobradoresPage {
  public data:any[] = [];

  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public modal: ModalController,
  ) {
    this.getAll();
  }

  getAll() {
    this.data = [];
    this.data = JSON.parse(localStorage.getItem('DemoCobrador'));
  }

  //AGREGAR
  openForm(parameter:any) {
    let chooseModal = this.modal.create(FormCobradoresPage, { parameter });
    chooseModal.onDidDismiss(data => {
      if(data != 'Close') {
        if(data == 'Aceptado') {
          this.getAll();
        }
      }      
    });
    chooseModal.present();
  }

  openCobros(parameter: any) {
    this.navCtrl.push(CobrosPage, { parameter })
  }

}
