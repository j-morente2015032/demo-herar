import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { ViewController } from '../../../../../node_modules/ionic-angular/navigation/view-controller';

@Component({
  selector: 'form-cobradores',
  templateUrl: 'form-cobradores.html'
})
export class FormCobradoresPage {
  public datalocal:any[] = [];
  public data = {
    id: '',
    nombre: '',
    correo: '',
    telefono: ''
  }
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public view: ViewController,
  ) {
    this.datalocal = JSON.parse(localStorage.getItem('DemoCobrador'));
  }

  public closeModal() {
    this.view.dismiss('Close');
  }

  public saveChanges() {
    if(this.datalocal) {
        this.datalocal.push(this.data)
    } else {
        this.datalocal = [this.data]
    }
    localStorage.setItem('DemoCobrador', JSON.stringify(this.datalocal));
    this.view.dismiss('Aceptado');
  }

}
