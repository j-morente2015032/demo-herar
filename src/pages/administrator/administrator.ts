import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { CobradoresPage } from './cobradores/cobradores';

@Component({
  selector: 'administrator',
  templateUrl: 'administrator.html'
})
export class AdministratorPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = CobradoresPage;
  pages: Array<{icon:string, ios:string, title: string, component: any}>;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public loading: LoadingController,) {
    this.loadModules();
  }

  loadModules() {
    this.pages = [
      { icon: 'md-apps', ios: 'ios-apps', title: 'Cobradores', component: CobradoresPage },
    ];
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logOut() {
    this.loading.create({
      content: "Cerrando Sesión...",
      duration: 1000
    }).present();
    location.reload()

  }
}
