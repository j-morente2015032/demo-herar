import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { FormCobrosPage } from './form-cobros/form-cobros';

@Component({
  selector: 'cobros',
  templateUrl: 'cobros.html'
})
export class CobrosPage {
  public data:any[] = [];
  public parameter:any;
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public modal: ModalController,
    public navParams: NavParams,
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getAll();
  }

  getAll() {
    this.data = [];
    this.data = JSON.parse(localStorage.getItem('DemoCobros'));
  }

  //AGREGAR
  openForm() {
    let chooseModal = this.modal.create(FormCobrosPage);
    chooseModal.onDidDismiss(data => {
      if(data != 'Close') {
        if(data == 'Aceptado') {
          this.getAll();
        }
      }      
    });
    chooseModal.present();
  }

  //AGREGAR
  detailForm(parameter:any) {
    let chooseModal = this.modal.create(FormCobrosPage, { parameter} );
    chooseModal.present();
  }


}
